# MIDLET-That-Demonstrates-SimpleItem-on-J2ME-Javaa
MIDLET That Demonstrates SimpleItem on J2ME Java
import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;

public class SimpleItemMIDlet
	extends MIDle
        implements 
	Form form = new Form("SimpleItemMIDlet");
	form.append(new SimpleItem("SimpleItem"));
	    
	Command c = new Command("Exit," Command.Exit, 0);
         form.addCommand(c);
         form.SetCommandListener(this);

        Display.getDisplay(this).setCurrent(form);
     }
     
     public void pauseApp(){}
	     
     public void destroyApp(boolean unconditional){}
	     
     public void commandAction(Command c, Displayable s) {
	if (c.getCommandType() == Command.EXIT)
	  notifyDestroyed();
     }
   }
